package com.gvelesiani.notes.diModules

import com.gvelesiani.notes.domain.useCases.*
import org.koin.dsl.module

val useCasesModule = module {
    factory {
        GetTodosListUseCase(get())
    }

    factory {
        AddTodoUseCase(get(), get())
    }

    factory {
        DeleteTodoUseCase(get())
    }

    factory {
        UpdateTodoUseCase(get())
    }

    factory {
        GetFavouriteNotesUseCase(get())
    }

    factory {
        TodoDoneUseCase(get())
    }

    factory {
        GetFromFirebaseAndStoreToRoom(get(),get())
    }

    factory {
        SaveTodosToRoom(get())
    }

    factory {
        GetTodoListSizeUseCase(get())
    }

    factory {
        UpdateFirestoreUseCase(get())
    }

    factory {
        AddTodoToFavourites(get())
    }

    factory {
        UpdateTodoPositionUseCase(get())
    }
}