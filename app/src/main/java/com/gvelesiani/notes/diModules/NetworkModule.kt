package com.gvelesiani.notes.diModules

import com.gvelesiani.notes.domain.dataProviders.global.GlobalDataProvider
import com.gvelesiani.notes.domain.dataProviders.global.GlobalDataProviderImpl
import org.koin.dsl.module

const val NODE_BASE_URL = "NODE_BASE_URL"
val networkModule = module {
//    single(named(NODE_BASE_URL)) {
//        androidContext().getString(R.string.base_url)
//    }
//
//    single {
//        GsonBuilder()
//            .create()
//    }
//
//    single {
//        GsonConverterFactory.create(get())
//    } bind Converter.Factory::class


//    single {
//        HttpLoggingInterceptor().apply {
//            level = HttpLoggingInterceptor.Level.BODY
//        }
//    }
//
//    single {
//        RxJava2CallAdapterFactory.create()
//    } bind CallAdapter.Factory::class
//
//    single {
//        ErrorHandlerInterceptor()
//    }

//    single {
//        OkHttpClient.Builder()
//            .apply {
//                if (BuildConfig.DEBUG) {
//                    addInterceptor(get<HttpLoggingInterceptor>())
//                }
//                addInterceptor(get<ErrorHandlerInterceptor>())
//            }
//            .build()
//    }
//
//    single {
//        GsonConverterFactory.create(get()) as Converter.Factory
//    }

//    single {
//        RxJava2CallAdapterFactory.create() as CallAdapter.Factory
//    }
//
//    single {
//        Retrofit.Builder()
//            .baseUrl(get<String>(named(NODE_BASE_URL)))
//            .client(get())
//            .addConverterFactory(get())
//            .addCallAdapterFactory(get())
//            .build()
//    }

//    single {
//        get<Retrofit>().create(GlobalDataProvider::class.java)
//    }

    factory {
        GlobalDataProviderImpl() as GlobalDataProvider
    }

}