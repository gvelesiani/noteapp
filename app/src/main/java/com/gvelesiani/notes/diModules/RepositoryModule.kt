package com.gvelesiani.notes.diModules

import com.gvelesiani.notes.domain.repository.TodoRepository
import com.gvelesiani.notes.domain.repository.TodoRepositoryImpl
import org.koin.dsl.bind
import org.koin.dsl.module

val repositoryModule = module {
    single {
        TodoRepositoryImpl(get(), get())
    } bind TodoRepository::class
}