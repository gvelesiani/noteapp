package com.gvelesiani.notes.diModules

import com.gvelesiani.notes.presentation.biometric_authentication.BiometricPromptViewModel
import com.gvelesiani.notes.presentation.MainViewModel
import com.gvelesiani.notes.presentation.account.AccountViewModel
import com.gvelesiani.notes.presentation.auth.AuthViewModel
import com.gvelesiani.notes.presentation.dashboard.DashboardViewModel
import com.gvelesiani.notes.presentation.favourites.FavouritesViewModel
import com.gvelesiani.notes.presentation.list.TodoListViewModel
import com.gvelesiani.notes.presentation.auth.login.LoginViewModel
import com.gvelesiani.notes.presentation.auth.register.RegisterViewModel
import com.gvelesiani.notes.presentation.splash.SplashViewModel
import com.gvelesiani.notes.presentation.splash.fragment.SplashFragmentViewModel
import com.gvelesiani.notes.presentation.update.TodoUpdateViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {

    viewModel { TodoUpdateViewModel(get(), get(), get()) }

    viewModel { TodoListViewModel(get(),get(),get(), get(), get(), get(), get()) }

    viewModel { MainViewModel() }

    viewModel { FavouritesViewModel(get(), get()) }

    viewModel { LoginViewModel() }

    viewModel { AccountViewModel() }

    viewModel { DashboardViewModel() }

    viewModel { AuthViewModel() }

    viewModel { RegisterViewModel() }

    viewModel {
        BiometricPromptViewModel()
    }

    viewModel {
        SplashFragmentViewModel()
    }

    viewModel {
        SplashViewModel()
    }
}