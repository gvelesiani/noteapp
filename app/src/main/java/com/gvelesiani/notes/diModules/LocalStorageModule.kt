package com.gvelesiani.notes.diModules

import androidx.preference.PreferenceManager
import androidx.room.Room
import com.gvelesiani.notes.domain.dataProviders.local.LocalDataProviderImpl
import com.gvelesiani.notes.domain.dataProviders.local.LocalDataProvider
import com.gvelesiani.notes.domain.dataProviders.local.database.TodoDatabase
import com.zuluft.mvvm.preferences.Preferences
import com.zuluft.mvvm.preferences.PreferencesImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.bind
import org.koin.dsl.module

val localStorageModule = module {
    single {
        Room.databaseBuilder(androidContext(), TodoDatabase::class.java, "todo_database")
            .fallbackToDestructiveMigration().allowMainThreadQueries().build()
    } bind TodoDatabase::class

    single {
        PreferenceManager.getDefaultSharedPreferences(androidContext())
    }

    single {
        LocalDataProviderImpl(
            get(),
            get()
        )
    } bind LocalDataProvider::class

    single {
        PreferencesImpl(get()) as Preferences
    }
}