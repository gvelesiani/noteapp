package com.gvelesiani.notes

import com.google.firebase.database.FirebaseDatabase
import com.gvelesiani.notes.diModules.*
import com.zuluft.mvvm.app.MvvmApplication
import org.koin.core.module.Module

class App : MvvmApplication() {
    override fun onCreate() {
        super.onCreate()
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
    }
    override fun provideModules(): List<Module> = listOf(
        viewModelsModule,
        localStorageModule,
        repositoryModule,
        useCasesModule,
        networkModule
    )

}