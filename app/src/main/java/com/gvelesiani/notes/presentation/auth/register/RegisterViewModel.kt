package com.gvelesiani.notes.presentation.auth.register

import com.zuluft.mvvm.viewModels.BaseViewModel

class RegisterViewModel : BaseViewModel<RegisterViewState>() {

    override fun getInitialState(): RegisterViewState {
        return RegisterViewState()
    }
}