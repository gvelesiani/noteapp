package com.gvelesiani.notes.presentation.favourites

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.useCases.AddTodoToFavourites
import com.gvelesiani.notes.domain.useCases.GetFavouriteNotesUseCase
import com.gvelesiani.notes.presentation.favourites.actions.GetAllFavouriteNotesAction
import com.gvelesiani.notes.presentation.favourites.actions.GoToDetailsAction
import com.gvelesiani.notes.presentation.favourites.actions.TodoFavouriteAction
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.common.extensions.async
import com.zuluft.mvvm.viewModels.BaseViewModel

class FavouritesViewModel(
    private val getFavouriteNotes: GetFavouriteNotesUseCase,
    private val addTodoToFavourites: AddTodoToFavourites
) : BaseViewModel<FavouritesViewState>() {

    override fun getInitialState(): FavouritesViewState {
        return FavouritesViewState()
    }

    fun goToDetailsFragment(model: TodoModel) {
        dispatchAction(GoToDetailsAction(model))
    }

    fun getFavourites() {
        registerDisposables(
            getFavouriteNotes.start()
                .map {
                    GetAllFavouriteNotesAction(
                        it
                    ) as ViewStateAction<FavouritesViewState>
                }
                .async()
                .subscribe(this::dispatchAction)
        )
    }

    fun addTodoToFavourites(model: TodoModel) {
        registerDisposables(
            addTodoToFavourites.start(model)
                .map {
                    TodoFavouriteAction() as ViewStateAction<FavouritesViewState>
                }
                .async()
                .subscribe(this::dispatchAction)
        )
    }
}