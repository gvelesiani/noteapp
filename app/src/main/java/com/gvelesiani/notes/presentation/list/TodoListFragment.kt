package com.gvelesiani.notes.presentation.list

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.core.widget.addTextChangedListener
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.app3null.common_code.extensions.notNullAndNotEmpty
import com.app3null.common_code.extensions.showThrowableMessage
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.gvelesiani.notes.R
import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.other.diffCallback.NoteListDiffCallback
import com.gvelesiani.notes.other.items.TodoItem
import com.gvelesiani.notes.other.navigate
import com.gvelesiani.notes.presentation.auth.AuthActivity
import com.gvelesiani.notes.presentation.update.TodoUpdateFragmentArgs
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.GenericFastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.common.extensions.gone
import com.zuluft.mvvm.common.extensions.notNull
import com.zuluft.mvvm.common.extensions.visible
import com.zuluft.mvvm.dialogs.Loader
import com.zuluft.mvvm.fragments.BaseFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.add_todo_alert_layout.view.*
import kotlinx.android.synthetic.main.custom_toolbar_layout.*
import kotlinx.android.synthetic.main.custom_toolbar_layout.view.*
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.TextStyle
import java.util.*


// ItemTouchCallback -> for drag and drop -
@LayoutResId(R.layout.fragment_list)
class TodoListFragment : BaseFragment<TodoListViewState, TodoListViewModel>() {

    //private lateinit var bottomSheetBehavior: BottomSheetBehavior<*>

    // Drag&Drop
//    private var dragCallback = SimpleDragCallback(this)
//    private var touchHelper = ItemTouchHelper(dragCallback)

    private val viewModel: TodoListViewModel by viewModel()
    private val db = Firebase.firestore.collection("users").document(Firebase.auth.currentUser!!.uid).collection("notes")

    private lateinit var fastItemAdapter: ItemAdapter<TodoItem>
    private var fastAdapter: GenericFastAdapter? = null
    private lateinit var auth: FirebaseAuth

    override fun provideViewModel(): TodoListViewModel {
        return viewModel
    }

    override fun reflectState(viewState: TodoListViewState) {

        viewState.showLoader?.getValue().notNull {
            registerDialog(Loader.show(requireContext()))
        }

        viewState.showError?.getValue().notNull {
            dismissAndClearDialogs()
            requireContext().showThrowableMessage(it)
        }

        viewState.goToDetailsFragment?.getValue().notNull {
            val action = TodoListFragmentDirections.actionTodoListFragmentToTodoUpdateFragment(it)
            findNavController().navigate(action)
        }

        viewState.goToAccount?.getValue().notNull {
            findNavController().navigate(R.id.action_todoListFragment_to_accountFragment)
        }

        viewState.todoList?.getValue().notNull { list ->
            val todoList = mutableListOf<TodoModel>()
            list.map {
                if (it.userUid == auth.currentUser?.uid) {
                    todoList.add(it)
                    drawDataAndSetSizeToTextView(todoList, requireActivity().toolbar.tvTasksNumber)
                }
            }
            if (todoList.isEmpty()) {
                requireActivity().toolbar.tvTasksNumber.text = "0 Tasks"
                showNoDataHints(true)
            }
            dismissAndClearDialogs()
        }
    }

    private fun showNoDataHints(isEmpty: Boolean) {
        when (isEmpty) {
            true -> {
                recyclerView.gone()
                noDataImageView.visible()
                noDataText.visible()
                noDataSubText.visible()
            }
            else -> {
                recyclerView.visible()
                noDataImageView.gone()
                noDataText.gone()
                noDataSubText.gone()
            }
        }
    }


    override fun renderView(savedInstanceState: Bundle?) {

        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        recyclerView.itemAnimator = null

        //setUpBottomSheet()

        requireActivity().toolbar.ivAvatar.setOnClickListener {
            findNavController().navigate(R.id.accountFragment, clearStack = false, disableReopening = true)
        }

        auth = FirebaseAuth.getInstance()
        setHasOptionsMenu(true)
        getItemsFromFirebaseAndStoreToRoom()

        // set up Recycler View
        setUpRecyclerView()
        setUpOnClickListeners()

        floatingActionButton.setOnClickListener {
            setUpDialog()
        }

        !searchView.isFocused
        searchView.addTextChangedListener {
            fastItemAdapter.filter(it)
            fastItemAdapter.itemFilter.filterPredicate =
                { item: TodoItem, constraint: CharSequence? ->
                    item.model.todoName.contains(constraint.toString(), ignoreCase = true)
                }
        }
    }

    private fun getItemsFromFirebaseAndStoreToRoom(){
        viewModel.getFromFirebaseAndStoreToRoom()
    }

    private fun setUpOnClickListeners(){
        fastAdapter.notNull { adapter ->
            adapter.addEventHook(TodoItem.TodoUpdateItemClick { _, item ->
                viewModel.goToUpdateFragment(item)
            })
        }

        fastAdapter.notNull { adapter ->
            adapter.addEventHook(TodoItem.TodoItemFavClick { position, item ->
                viewModel.addToFavouritesRemote(item)
                fastAdapter.notNull {
                    it.notifyAdapterItemChanged(position)
                }
            })
        }

        fastAdapter.notNull { adapter ->
            adapter.addEventHook(TodoItem.TodoItemDoneClick { position, item ->
                viewModel.todoDone(item)

                fastAdapter.notNull {
                    it.notifyAdapterItemChanged(position)
                }
            })
        }

        fastAdapter.notNull { adapter ->
            adapter.addEventHook(TodoItem.TodoDeleteClick { _, item ->
                deleteTodo(item)
            })
        }
    }

    private fun deleteTodo(todoModel: TodoModel) {
        val builder = android.app.AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Yes") { _, _ ->
            viewModel.deleteTodo(todoModel)
            delete(todoModel)
        }
        builder.setNegativeButton("No") { _, _ -> }
        builder.setTitle("Delete this todo?")
        builder.setMessage("Are you sure you want to delete this note?")
        builder.create().show()
    }

    private fun delete(todoModel: TodoModel){
        db.document(todoModel.id).delete()
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.list_menu, menu)
        val search = menu.findItem(R.id.menu_search)
        val searchView = search.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                //dragCallback.setIsDragEnabled(false)
                fastItemAdapter.filter(s)
                fastItemAdapter.itemFilter.filterPredicate =
                    { item: TodoItem, constraint: CharSequence? ->
                        item.model.todoName.contains(constraint.toString(), ignoreCase = true)
                    }
                return true
            }

            override fun onQueryTextChange(s: String): Boolean {
                fastItemAdapter.filter(s)
                //dragCallback.setIsDragEnabled(TextUtils.isEmpty(s))
                fastItemAdapter.itemFilter.filterPredicate =
                    { item: TodoItem, constraint: CharSequence? ->
                        item.model.todoName.contains(constraint.toString(), ignoreCase = true)
                    }
                return true
            }
        })

        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.menu_log_out) {
            Firebase.auth.signOut()
            val intent = Intent(requireActivity(), AuthActivity::class.java)
            startActivity(intent)
            activity?.onBackPressed()
        }
        if (item.itemId == R.id.menu_account) {
            goToAccountFragment()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUpRecyclerView() {
        if (fastAdapter == null) {
            fastItemAdapter = ItemAdapter()
            fastAdapter = FastAdapter.Companion.with(
                listOf(fastItemAdapter)
            )
        }
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = fastAdapter
        recyclerView.itemAnimator = null
        //touchHelper.attachToRecyclerView(recyclerView)
    }

    @SuppressLint("SetTextI18n")
    private fun drawDataAndSetSizeToTextView(list: List<TodoModel>, textView: TextView) {
        fastItemAdapter.clear()
        fastItemAdapter.notNull { adapter ->
            list.notNullAndNotEmpty { list ->
                val old = adapter.adapterItems
                val result = FastAdapterDiffUtil.calculateDiff(
                    adapter,
                    old.plus(list.map { TodoItem(it) }),
                    NoteListDiffCallback()
                )

                FastAdapterDiffUtil[adapter] = result
            }
            showNoDataHints(adapter.adapterItems.isEmpty())
        }

        textView.text = list.size.toString() + " Tasks"
        //(recyclerView.layoutManager as LinearLayoutManager).smoothScrollToPosition(recyclerView, null, list.size)
    }

    private fun goToAccountFragment() {
        viewModel.goToAccountFragment()
    }

    private fun setUpDialog() {
        val mDialogView =
            LayoutInflater.from(requireContext()).inflate(R.layout.add_todo_alert_layout, null)
        val mBuilder = AlertDialog.Builder(requireContext())
            .setView(mDialogView)
            .setCancelable(false)


        mDialogView.setBackgroundResource(android.R.color.transparent)
        val mAlertDialog = mBuilder.show()


        mDialogView.addButton.setOnClickListener {
            if (!viewModel.checkFields(
                    requireContext(),
                    mDialogView.alertDialogEt.text.toString()
                )
            ) {
                viewModel.addTodo(
                    TodoModel(
                        todoName = mDialogView.alertDialogEt.text.toString(),
                        favourite = false, done = false, userUid = auth.currentUser!!.uid
                    )
                )
                mAlertDialog.dismiss()
            }
        }
        mDialogView.btnCancel.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    // Drag&Drop
//    override fun itemTouchDropped(oldPosition: Int, newPosition: Int) {
//        viewModel.updateTodoPosition(viewModel.todoList[oldPosition], newPosition)
//    }
//
//    override fun itemTouchOnMove(oldPosition: Int, newPosition: Int): Boolean {
//        DragDropUtil.onMove(fastItemAdapter, oldPosition, newPosition)
//        return true
//    }



//    private fun setUpBottomSheet() {
//
//        val bottomSheet: LinearLayout = bottom_sheet_behavior_id as LinearLayout
//        bottomSheetBehavior =
//            BottomSheetBehavior.from(bottomSheet)
//
//        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetCallback() {
//            override fun onSlide(p0: View, p1: Float) {
//
//            }
//
//            override fun onStateChanged(p0: View, p1: Int) {
//
//            }
//        })
//
//        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
//
//
//        bottomSheet.btnBottomSheetAddTodo.setOnClickListener {
//            if (!viewModel.checkFields(
//                    requireContext(),
//                    bottomSheet.etBottomSheetAddTodoName.text.toString()
//                )
//            ) {
//                viewModel.addTodo(
//                    TodoModel(
//                        todoName = bottomSheet.etBottomSheetAddTodoName.text.toString(),
//                        favourite = false, done = false, userUid = auth.currentUser!!.uid
//                    )
//                )
//                bottomSheet.etBottomSheetAddTodoName.setText("")
//                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
//
//            }
//        }
//
//    }
//
//    private fun dismissKeyboard(activity: Activity) {
//        val imm =
//            activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        if (null != activity.currentFocus) imm.hideSoftInputFromWindow(
//            activity.currentFocus!!
//                .applicationWindowToken, 0
//        )
//    }

}


