package com.gvelesiani.notes.presentation.list.actions

import com.gvelesiani.notes.presentation.list.TodoListViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class GetListSizeAction(private val size: Int): ViewStateAction<TodoListViewState> {
    override fun newState(oldState: TodoListViewState): TodoListViewState {
        return oldState.copy(getSize = DisposableValue(size))
    }
}