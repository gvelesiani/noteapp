package com.gvelesiani.notes.presentation.list

import android.content.Context
import android.util.Log.d
import android.widget.Toast
import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.useCases.*
import com.gvelesiani.notes.other.async
import com.gvelesiani.notes.presentation.list.actions.*
import com.gvelesiani.notes.presentation.update.TodoUpdateViewState
import com.gvelesiani.notes.presentation.update.actions.DeleteTodoAction
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.common.extensions.async
import com.zuluft.mvvm.viewModels.BaseViewModel

class TodoListViewModel(
    private val getTodoListUseCase: GetTodosListUseCase,
    private val addTodoToFavourites: AddTodoToFavourites,
    private val todoDoneUseCase: TodoDoneUseCase,
    private val getFromFirebaseAndStoreToRoom: GetFromFirebaseAndStoreToRoom,
    private val addTodoUseCase: AddTodoUseCase,
    private val deleteTodoUseCase: DeleteTodoUseCase,
    private val updateTodoPositionUseCase: UpdateTodoPositionUseCase
) : BaseViewModel<TodoListViewState>() {

    lateinit var todoList: MutableList<TodoModel>
    override fun getInitialState(): TodoListViewState {
        return TodoListViewState()
    }

    fun goToUpdateFragment(model: TodoModel) {
        dispatchAction(GoToUpdateAction(model))
    }

    fun goToAccountFragment() {
        dispatchAction(GoToAccountAction())
    }

//    fun updateTodoPosition(todo: TodoModel, newPosition: Int){
//        d("dbg", "register")
//        registerDisposables(
//            updateTodoPositionUseCase.start(arg = Pair(todo, newPosition))
//                .async()
//                .subscribe {
//                    d("dbg", "subscribe")
//                    dispatchAction(PositionAction())
//                }
//        )
//    }

    fun deleteTodo(model: TodoModel) {
        registerDisposables(
            deleteTodoUseCase.start(model)
                .map {
                    DeleteAction() as ViewStateAction<TodoListViewState>
                }
                .async()
                .subscribe(this::dispatchAction)
        )
    }

    fun getFromFirebaseAndStoreToRoom(){
        registerDisposables(
            getFromFirebaseAndStoreToRoom.start()
                .flatMap {
                    getTodoListUseCase.start()
                        .map { list ->
                            todoList = list as MutableList<TodoModel>
                            GetListAction(list) as ViewStateAction<TodoListViewState>
                        }
                }
                .onErrorReturn { ShowErrorAction(it) }
                .startWith(ShowLoaderAction())
                .async()
                .subscribe(this::dispatchAction)
        )
    }

    fun addToFavouritesRemote(todo: TodoModel){
        registerDisposables(
            addTodoToFavourites.start(todo)
                .map {
                    AddTodoToFavouritesAction() as ViewStateAction<TodoListViewState>
                }
                .async()
                .subscribe(this::dispatchAction)
        )
    }


    fun checkFields(context: Context, name: String): Boolean {
        return if (name.isEmpty()) {
            Toast.makeText(context, "Oops, you forgot to write something..", Toast.LENGTH_SHORT)
                .show()
            true
        } else {
            false
        }
    }

    fun todoDone(model: TodoModel){
        registerDisposables(
            todoDoneUseCase.start(model)
                .map {
                    TodoDoneAction() as ViewStateAction<TodoListViewState>
                }
                .async()
                .subscribe(this::dispatchAction)
        )
    }

    fun addTodo(model: TodoModel){
        registerDisposables(
            addTodoUseCase.start(model)
                .subscribe {
                    dispatchAction(AddToRoomAction())
                }
        )
    }
}