package com.gvelesiani.notes.presentation.list.actions

import com.gvelesiani.notes.presentation.list.TodoListViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class ShowErrorAction(
    private val throwable: Throwable
) : ViewStateAction<TodoListViewState> {
    override fun newState(oldState: TodoListViewState): TodoListViewState {
        return oldState.copy(showError = DisposableValue(throwable))
    }
}