package com.gvelesiani.notes.presentation

import com.zuluft.mvvm.viewModels.BaseViewModel

class MainViewModel : BaseViewModel<MainViewState>() {

    override fun getInitialState(): MainViewState {
        return MainViewState()
    }
}