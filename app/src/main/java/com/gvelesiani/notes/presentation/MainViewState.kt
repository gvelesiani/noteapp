package com.gvelesiani.notes.presentation

import com.zuluft.mvvm.models.DisposableValue

data class MainViewState(
    val goToHomeScreen: DisposableValue<Boolean>? = null
)