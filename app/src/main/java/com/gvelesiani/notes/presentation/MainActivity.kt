package com.gvelesiani.notes.presentation

import android.os.Bundle
import android.widget.TextView
import com.google.firebase.FirebaseApp
import com.gvelesiani.notes.R
import com.zuluft.mvvm.activities.BaseActivity
import com.zuluft.mvvm.common.LayoutResId
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_toolbar_layout.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.TextStyle
import java.util.*


@LayoutResId(R.layout.activity_main)
class MainActivity : BaseActivity<MainViewState, MainViewModel>() {
    private val viewModel: MainViewModel by viewModel()

    override fun provideViewModel(): MainViewModel {
        return viewModel
    }

    override fun reflectState(viewState: MainViewState) {

    }



    override fun renderView(savedInstanceState: Bundle?) {
        FirebaseApp.initializeApp(this)
        getDate(toolbar.tvCurrentDayOfWeek, toolbar.tvCurrentMonth, toolbar.tvCurrentDayDate)

        //setupActionBarWithNavController(findNavController(R.id.fragment))
    }

    private fun getDate(day: TextView, month: TextView, dayOfMonth: TextView){
        val date: LocalDate = LocalDate.now()
        val dow: DayOfWeek = date.dayOfWeek
        val dayName = dow.getDisplayName(TextStyle.FULL, Locale.ENGLISH)
        val monthName = date.month.getDisplayName(TextStyle.FULL, Locale.ENGLISH)
        day.text = dayName
        month.text = monthName
        dayOfMonth.text = date.dayOfMonth.toString()
    }

//    override fun onSupportNavigateUp(): Boolean {
//        val navController = findNavController(R.id.fragment)
//        return navController.navigateUp() || super.onSupportNavigateUp()
//    }
}