package com.gvelesiani.notes.presentation.list.actions

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.presentation.list.TodoListViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class GoToUpdateAction(private val model: TodoModel): ViewStateAction<TodoListViewState> {
    override fun newState(oldState: TodoListViewState): TodoListViewState {
        return oldState.copy(goToDetailsFragment = DisposableValue(model))
    }
}