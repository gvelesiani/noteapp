package com.gvelesiani.notes.presentation.list.actions

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.presentation.list.TodoListViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class GetListAction(private val list: List<TodoModel>): ViewStateAction<TodoListViewState> {
    override fun newState(oldState: TodoListViewState): TodoListViewState {
        return oldState.copy(todoList = DisposableValue(list))
    }
}