package com.gvelesiani.notes.presentation.dashboard

import com.zuluft.mvvm.models.DisposableValue

data class DashboardViewState(
    val showLoader: DisposableValue<Boolean>? = null,
    val showError: DisposableValue<Throwable>? = null,
    val goTo: DisposableValue<Boolean>? = null
)
