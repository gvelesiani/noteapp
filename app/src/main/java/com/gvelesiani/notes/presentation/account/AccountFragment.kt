package com.gvelesiani.notes.presentation.account

import android.content.Intent
import android.os.Bundle
import com.app3null.common_code.extensions.showThrowableMessage
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.gvelesiani.notes.R
import com.gvelesiani.notes.presentation.auth.AuthActivity
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.common.extensions.notNull
import com.zuluft.mvvm.dialogs.Loader
import com.zuluft.mvvm.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_account.*
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.fragment_account)
class AccountFragment : BaseFragment<AccountViewState, AccountViewModel>() {

    private val viewModel: AccountViewModel by viewModel()

    override fun provideViewModel(): AccountViewModel {
        return viewModel
    }

    override fun reflectState(viewState: AccountViewState) {
        viewState.showLoader?.getValue().notNull {
            registerDialog(Loader.show(requireContext()))
        }

        viewState.showError?.getValue().notNull {
            dismissAndClearDialogs()
            requireContext().showThrowableMessage(it)
        }
    }

    override fun renderView(savedInstanceState: Bundle?) {
        val user = Firebase.auth.currentUser!!
        showDetails(user)

        btnLogOut.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(requireContext(), AuthActivity::class.java)
            startActivity(intent)
        }
    }

    private fun showDetails(user: FirebaseUser?){
        tvFullName.text = user!!.displayName
        tvEmail.text = user.email
        Glide.with(requireContext()).load(FirebaseAuth.getInstance().currentUser!!.photoUrl).into(ivAccountAvatar)
    }
}