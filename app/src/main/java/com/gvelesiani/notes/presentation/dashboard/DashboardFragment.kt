package com.gvelesiani.notes.presentation.dashboard

import android.os.Bundle
import com.app3null.common_code.extensions.showThrowableMessage
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.fragments.BaseFragment
import com.gvelesiani.notes.R
import com.zuluft.mvvm.common.extensions.notNull
import com.zuluft.mvvm.dialogs.Loader
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.fragment_dashboard)
class DashboardFragment : BaseFragment<DashboardViewState, DashboardViewModel>() {

    private val viewModel: DashboardViewModel by viewModel()

    override fun provideViewModel(): DashboardViewModel {
        return viewModel
    }

    override fun reflectState(viewState: DashboardViewState) {
        viewState.showLoader?.getValue().notNull {
            registerDialog(Loader.show(requireContext()))
        }

        viewState.showError?.getValue().notNull {
            dismissAndClearDialogs()
            requireContext().showThrowableMessage(it)
        }
    }

    override fun renderView(savedInstanceState: Bundle?) {

    }
}