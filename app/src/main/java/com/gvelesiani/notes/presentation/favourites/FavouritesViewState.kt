package com.gvelesiani.notes.presentation.favourites

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.zuluft.mvvm.models.DisposableValue

data class FavouritesViewState(
    val showLoader: DisposableValue<Boolean>? = null,
    val showError: DisposableValue<Throwable>? = null,
    val goTo: DisposableValue<Boolean>? = null,
    val favouriteList: DisposableValue<List<TodoModel>>? = null,
    val todoFavourite: DisposableValue<Boolean>? = null,
    val goToDetailsFragment: DisposableValue<TodoModel>? = null
)
