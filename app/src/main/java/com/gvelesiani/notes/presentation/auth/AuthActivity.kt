package com.gvelesiani.notes.presentation.auth

import android.os.Bundle
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.activities.BaseActivity
import com.gvelesiani.notes.R
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.activity_auth)
class AuthActivity : BaseActivity<AuthViewState, AuthViewModel>() {

    private val viewModel: AuthViewModel by viewModel()

    override fun provideViewModel(): AuthViewModel {
        return viewModel
    }

    override fun reflectState(viewState: AuthViewState) {

    }

    override fun renderView(savedInstanceState: Bundle?) {
//        val viewPager = findViewById<ViewPager>(R.id.viewPager)
//        val pagerAdapter =
//            PagerAdapter(
//                supportFragmentManager,
//                1
//            )
//
//        pagerAdapter.addFragment(LoginFragment())
//        pagerAdapter.addFragment(RegisterFragment())
//
//        viewPager.adapter = pagerAdapter
    }
}