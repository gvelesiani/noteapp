package com.gvelesiani.notes.presentation.splash.fragment

import com.zuluft.mvvm.models.DisposableValue

data class SplashFragmentViewState(
    val showLoader: DisposableValue<Boolean>? = null,
    val showError: DisposableValue<Throwable>? = null,
    val goTo: DisposableValue<Boolean>? = null,
    val goToMainScreen: DisposableValue<Boolean>? = null

)
