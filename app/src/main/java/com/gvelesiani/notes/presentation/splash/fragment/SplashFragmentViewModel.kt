package com.gvelesiani.notes.presentation.splash.fragment

import com.gvelesiani.notes.presentation.splash.fragment.actions.GoToMainScreenAction
import com.zuluft.mvvm.common.extensions.async
import com.zuluft.mvvm.viewModels.BaseViewModel
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class SplashFragmentViewModel : BaseViewModel<SplashFragmentViewState>() {

    override fun getInitialState(): SplashFragmentViewState {
        return SplashFragmentViewState()
    }

    init {
        registerDisposables(
            Observable.timer(2500L, TimeUnit.MILLISECONDS)
                .map {
                    GoToMainScreenAction()
                }
                .async()
                .subscribe(this::dispatchAction)
        )
    }
}