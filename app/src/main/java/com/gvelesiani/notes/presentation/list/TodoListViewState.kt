package com.gvelesiani.notes.presentation.list

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.zuluft.mvvm.models.DisposableValue

data class TodoListViewState(
    val todoList: DisposableValue<List<TodoModel>>? = null,
    val showLoader: DisposableValue<Boolean>? = null,
    val showError: DisposableValue<Throwable>? = null,
    val goTo: DisposableValue<Boolean>? = null,
    val goToDetailsFragment: DisposableValue<TodoModel>? = null,
    val todoFavourite: DisposableValue<Boolean>? = null,
    val todoDone: DisposableValue<Boolean>? = null,
    val addTodo: DisposableValue<Boolean>? = null,
    val goToAccount: DisposableValue<Boolean>? = null,
    val getSize: DisposableValue<Int>? = null,
    val position: DisposableValue<Boolean>? = null,
    val deleteTodo: DisposableValue<Boolean>? = null
)
