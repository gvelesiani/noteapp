package com.gvelesiani.notes.presentation.account

import com.zuluft.mvvm.models.DisposableValue

data class AccountViewState(
    val showLoader: DisposableValue<Boolean>? = null,
    val showError: DisposableValue<Throwable>? = null,
    val goTo: DisposableValue<Boolean>? = null
)
