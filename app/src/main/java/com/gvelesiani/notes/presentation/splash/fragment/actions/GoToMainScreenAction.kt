package com.gvelesiani.notes.presentation.splash.fragment.actions

import com.gvelesiani.notes.presentation.splash.fragment.SplashFragmentViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue


internal class GoToMainScreenAction : ViewStateAction<SplashFragmentViewState> {
    override fun newState(oldState: SplashFragmentViewState): SplashFragmentViewState {
        return oldState.copy(goToMainScreen = DisposableValue(true))
    }
}