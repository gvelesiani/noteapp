package com.gvelesiani.notes.presentation.splash

import com.zuluft.mvvm.viewModels.BaseViewModel

class SplashViewModel : BaseViewModel<SplashViewState>() {

    override fun getInitialState(): SplashViewState {
        return SplashViewState
    }
}