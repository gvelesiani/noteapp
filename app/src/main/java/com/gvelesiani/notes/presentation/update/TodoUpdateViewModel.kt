package com.gvelesiani.notes.presentation.update

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.useCases.DeleteTodoUseCase
import com.gvelesiani.notes.domain.useCases.UpdateFirestoreUseCase
import com.gvelesiani.notes.domain.useCases.UpdateTodoUseCase
import com.gvelesiani.notes.presentation.update.actions.DeleteTodoAction
import com.gvelesiani.notes.presentation.update.actions.GoToTodoListFragment
import com.gvelesiani.notes.presentation.update.actions.UpdateTodoAction
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.common.extensions.async
import com.zuluft.mvvm.viewModels.BaseViewModel

class TodoUpdateViewModel(
    private val deleteTodoUseCase: DeleteTodoUseCase,
    private val updateTodoUseCase: UpdateTodoUseCase,
    private val updateFirestoreUseCase: UpdateFirestoreUseCase
) : BaseViewModel<TodoUpdateViewState>() {

    override fun getInitialState(): TodoUpdateViewState {
        return TodoUpdateViewState()
    }

    fun goToListAfterUpdate(){
        dispatchAction(GoToTodoListFragment())
    }

    fun deleteTodo(model: TodoModel) {
        registerDisposables(
            deleteTodoUseCase.start(model)
                .map {
                    DeleteTodoAction() as ViewStateAction<TodoUpdateViewState>
                }
                .async()
                .subscribe(this::dispatchAction)
        )
    }

    fun updateFirestore(model: TodoModel){
        registerDisposables(
            updateFirestoreUseCase.start(model)
                .subscribe {
                    UpdateTodoAction()
                }
        )
    }

    fun updateTodo(model: TodoModel){
        registerDisposables(
            updateTodoUseCase.start(model)
                .map {
                    UpdateTodoAction() as ViewStateAction<TodoUpdateViewState>
                }
                .async()
                .subscribe(this::dispatchAction)
        )
    }


}