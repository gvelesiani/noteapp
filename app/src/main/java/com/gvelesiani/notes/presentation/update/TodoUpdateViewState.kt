package com.gvelesiani.notes.presentation.update

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.zuluft.mvvm.models.DisposableValue

data class TodoUpdateViewState(
    val showLoader: DisposableValue<Boolean>? = null,
    val showError: DisposableValue<Throwable>? = null,
    val goTo: DisposableValue<Boolean>? = null,
    val deleteTodo: DisposableValue<Boolean>? = null,
    val updateTodo: DisposableValue<Boolean>? = null,
    val goToList: DisposableValue<Boolean>? = null,
    val goToDetailsAfterUpdate: DisposableValue<TodoModel>? = null
)
