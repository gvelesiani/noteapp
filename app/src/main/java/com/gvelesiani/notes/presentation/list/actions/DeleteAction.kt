package com.gvelesiani.notes.presentation.list.actions

import com.gvelesiani.notes.presentation.list.TodoListViewState
import com.gvelesiani.notes.presentation.update.TodoUpdateViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class DeleteAction : ViewStateAction<TodoListViewState> {
    override fun newState(oldState: TodoListViewState): TodoListViewState {
        return oldState.copy(deleteTodo = DisposableValue(true))
    }
}