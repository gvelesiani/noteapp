package com.gvelesiani.notes.presentation.list.actions

import com.gvelesiani.notes.presentation.list.TodoListViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class ShowLoaderAction : ViewStateAction<TodoListViewState> {
    override fun newState(oldState: TodoListViewState): TodoListViewState {
        return oldState.copy(showLoader = DisposableValue(true))
    }
}