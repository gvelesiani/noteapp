package com.gvelesiani.notes.presentation.splash.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.app3null.common_code.extensions.showThrowableMessage
import com.google.firebase.auth.FirebaseAuth
import com.gvelesiani.notes.R
import com.gvelesiani.notes.presentation.MainActivity
import com.gvelesiani.notes.presentation.auth.AuthActivity
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.fragments.BaseFragment
import com.zuluft.mvvm.common.extensions.notNull
import com.zuluft.mvvm.dialogs.Loader
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.fragment_splash)
class SplashFragment : BaseFragment<SplashFragmentViewState, SplashFragmentViewModel>() {

    lateinit var auth: FirebaseAuth
    private val fragmentViewModel: SplashFragmentViewModel by viewModel()

    override fun provideViewModel(): SplashFragmentViewModel {
        return fragmentViewModel
    }

    override fun reflectState(viewState: SplashFragmentViewState) {
        viewState.showLoader?.getValue().notNull {
            registerDialog(Loader.show(requireContext()))
        }

        viewState.showError?.getValue().notNull {
            dismissAndClearDialogs()
            requireContext().showThrowableMessage(it)
        }

        viewState.goToMainScreen?.getValue().notNull {
            goToMainScreenOrAuth()
        }
    }

    override fun renderView(savedInstanceState: Bundle?) {
        auth = FirebaseAuth.getInstance()
    }

    private fun goToMainScreenOrAuth(){
        val currentUser = auth.currentUser
        if(currentUser != null){
            intentFunction(MainActivity())
        } else {
            intentFunction(AuthActivity())
        }
    }

    private fun intentFunction(activity: Activity){
        val intent = Intent(context, activity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        requireActivity().finish()
    }

}