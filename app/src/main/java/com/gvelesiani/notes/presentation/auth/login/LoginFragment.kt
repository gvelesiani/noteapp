package com.gvelesiani.notes.presentation.auth.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.gvelesiani.notes.R
import com.gvelesiani.notes.presentation.MainActivity
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.fragments.BaseFragment
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.activity_login)
class LoginFragment : BaseFragment<LoginViewState, LoginViewModel>() {
    private lateinit var auth: FirebaseAuth
    private val viewModel: LoginViewModel by viewModel()

    override fun provideViewModel(): LoginViewModel {
        return viewModel
    }

    override fun reflectState(viewState: LoginViewState) {

    }

    override fun renderView(savedInstanceState: Bundle?) {

        auth = FirebaseAuth.getInstance()

        btnLogin.setOnClickListener {
            if (etLoginEmailField.text.toString()
                    .isNotEmpty() && etLoginPasswordField.text.toString().isNotEmpty()
            ) {
                auth.signInWithEmailAndPassword(
                    etLoginEmailField.text.toString(),
                    etLoginPasswordField.text.toString()
                ).addOnCompleteListener {

                    if (it.isSuccessful) {
                        val intent = Intent(requireContext(), MainActivity::class.java)
                        startActivity(intent)
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Auth failed",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            } else {
                Toast.makeText(
                    requireContext(), "Authentication failed.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        signUpTV.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

    }

}