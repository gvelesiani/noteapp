package com.gvelesiani.notes.presentation.update.actions

import com.gvelesiani.notes.presentation.update.TodoUpdateViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class DeleteTodoAction : ViewStateAction<TodoUpdateViewState> {
    override fun newState(oldState: TodoUpdateViewState): TodoUpdateViewState {
        return oldState.copy(deleteTodo = DisposableValue(true))
    }
}