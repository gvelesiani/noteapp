package com.gvelesiani.notes.presentation.dashboard

import com.zuluft.mvvm.viewModels.BaseViewModel

class DashboardViewModel : BaseViewModel<DashboardViewState>() {

    override fun getInitialState(): DashboardViewState {
        return DashboardViewState()
    }
}