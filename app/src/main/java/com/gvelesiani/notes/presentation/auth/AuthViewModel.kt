package com.gvelesiani.notes.presentation.auth

import com.zuluft.mvvm.viewModels.BaseViewModel

class AuthViewModel : BaseViewModel<AuthViewState>() {

    override fun getInitialState(): AuthViewState {
        return AuthViewState
    }
}