package com.gvelesiani.notes.presentation.splash

import android.os.Bundle
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.activities.BaseActivity
import com.gvelesiani.notes.R
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.activity_splash)
class SplashActivity : BaseActivity<SplashViewState, SplashViewModel>() {

    private val viewModel: SplashViewModel by viewModel()

    override fun provideViewModel(): SplashViewModel {
        return viewModel
    }

    override fun reflectState(viewState: SplashViewState) {

    }

    override fun renderView(savedInstanceState: Bundle?) {

    }
}