package com.gvelesiani.notes.presentation.auth.register

import com.zuluft.mvvm.models.DisposableValue

data class RegisterViewState(
    val showLoader: DisposableValue<Boolean>? = null,
    val showError: DisposableValue<Throwable>? = null,
    val goTo: DisposableValue<Boolean>? = null
)
