package com.gvelesiani.notes.presentation.biometric_authentication

import com.zuluft.mvvm.viewModels.BaseViewModel

class BiometricPromptViewModel : BaseViewModel<BiometricPromptViewState>() {

    override fun getInitialState(): BiometricPromptViewState {
        return BiometricPromptViewState
    }
}