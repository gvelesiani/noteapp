package com.gvelesiani.notes.presentation.auth.login

import com.zuluft.mvvm.viewModels.BaseViewModel

class LoginViewModel : BaseViewModel<LoginViewState>() {

    override fun getInitialState(): LoginViewState {
        return LoginViewState
    }
}