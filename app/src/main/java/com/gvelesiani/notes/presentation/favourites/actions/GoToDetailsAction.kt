package com.gvelesiani.notes.presentation.favourites.actions

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.presentation.favourites.FavouritesViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class GoToDetailsAction(private val model: TodoModel): ViewStateAction<FavouritesViewState> {
    override fun newState(oldState: FavouritesViewState): FavouritesViewState {
        return oldState.copy(goToDetailsFragment = DisposableValue(model))
    }
}