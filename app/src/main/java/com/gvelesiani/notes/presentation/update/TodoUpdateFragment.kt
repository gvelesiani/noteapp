package com.gvelesiani.notes.presentation.update

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.navigation.fragment.findNavController
import com.app3null.common_code.extensions.showThrowableMessage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.gvelesiani.notes.R
import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.common.extensions.notNull
import com.zuluft.mvvm.dialogs.Loader
import com.zuluft.mvvm.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_update.*
import org.koin.android.viewmodel.ext.android.viewModel


@LayoutResId(R.layout.fragment_update)
class TodoUpdateFragment : BaseFragment<TodoUpdateViewState, TodoUpdateViewModel>() {
    private val viewModel: TodoUpdateViewModel by viewModel()
    private val db = Firebase.firestore.collection("users").document(Firebase.auth.currentUser!!.uid).collection("notes")
    private lateinit var auth: FirebaseAuth
    override fun provideViewModel(): TodoUpdateViewModel {
        return viewModel
    }

    override fun reflectState(viewState: TodoUpdateViewState) {

        viewState.showLoader?.getValue().notNull {
            registerDialog(Loader.show(requireContext()))
        }

        viewState.showError?.getValue().notNull {
            dismissAndClearDialogs()
            requireContext().showThrowableMessage(it)
        }

        viewState.updateTodo?.getValue().notNull {
            findNavController().navigate(R.id.action_todoUpdateFragment_to_todoListFragment)
        }

        viewState.goToList?.getValue().notNull {
            findNavController().navigate(R.id.action_todoUpdateFragment_to_todoListFragment)
        }
    }

    override fun renderView(savedInstanceState: Bundle?) {

        auth = FirebaseAuth.getInstance()
        setHasOptionsMenu(true)
        val args = TodoUpdateFragmentArgs.fromBundle(requireArguments()).listTodo
        editTodoName.setText(args.todoName)

        btnUpdateTodo.setOnClickListener {
            updateTodo(args)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_delete) {
            deleteTodo()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteTodo() {
        val args = TodoUpdateFragmentArgs.fromBundle(requireArguments()).listTodo
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Yes") { _, _ ->
            viewModel.deleteTodo(args)
            delete(args)
            viewModel.goToListAfterUpdate()
        }
        builder.setNegativeButton("No") { _, _ -> }
        builder.setTitle("Delete this todo?")
        builder.setMessage("Are you sure you want to delete this note?")
        builder.create().show()
    }



    private fun updateTodo(todoModel: TodoModel) {
        val model = TodoModel(
            id = todoModel.id,
            position = todoModel.position,
            todoName = editTodoName.text.toString()
        )
        viewModel.updateFirestore(model)
        viewModel.goToListAfterUpdate()
    }

    private fun delete(todoModel: TodoModel){
        db.document(todoModel.id).delete()
    }
}