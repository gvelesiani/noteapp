package com.gvelesiani.notes.presentation.account

import com.zuluft.mvvm.viewModels.BaseViewModel

class AccountViewModel : BaseViewModel<AccountViewState>() {

    override fun getInitialState(): AccountViewState {
        return AccountViewState()
    }
}