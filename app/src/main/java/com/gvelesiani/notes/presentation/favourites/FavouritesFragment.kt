package com.gvelesiani.notes.presentation.favourites

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.app3null.common_code.extensions.showThrowableMessage
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.fragments.BaseFragment
import com.gvelesiani.notes.R
import com.gvelesiani.notes.other.items.TodoItem
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.zuluft.mvvm.common.extensions.notNull
import com.zuluft.mvvm.common.extensions.visible
import com.zuluft.mvvm.dialogs.Loader
import kotlinx.android.synthetic.main.fragment_favourites.*
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.fragment_favourites)
class FavouritesFragment : BaseFragment<FavouritesViewState, FavouritesViewModel>() {

    private val viewModel: FavouritesViewModel by viewModel()
    private var itemAdapter = ItemAdapter<TodoItem>()
    private var fastAdapter = FastAdapter.with(itemAdapter)

    override fun provideViewModel(): FavouritesViewModel {
        return viewModel
    }

    override fun reflectState(viewState: FavouritesViewState) {
        viewState.showLoader?.getValue().notNull {
            registerDialog(Loader.show(requireContext()))
        }

        viewState.showError?.getValue().notNull {
            dismissAndClearDialogs()
            requireContext().showThrowableMessage(it)
        }

        viewState.favouriteList?.getValue().notNull { list ->
            itemAdapter.clear()
            if (list.isEmpty()) {
                noDataImageView.visible()
                noDataSubText.visible()
                noDataText.visible()
            } else {
                itemAdapter.clear()
                list.map {
                    itemAdapter.add(
                        listOf(TodoItem(it))
                    )
                }
            }
        }

        viewState.goToDetailsFragment?.getValue().notNull {
            val action = FavouritesFragmentDirections.actionFavouritesFragmentToTodoUpdateFragment(it)
            findNavController().navigate(action)
        }
    }

    override fun renderView(savedInstanceState: Bundle?) {
        viewModel.getFavourites()
        setUpRecyclerView()
    }

    private fun setUpRecyclerView() {
        itemAdapter = ItemAdapter()
        fastAdapter = FastAdapter.Companion.with(
            listOf(itemAdapter)
        )
        favRecyclerView.layoutManager =
            StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

        favRecyclerView.adapter = fastAdapter
        fastAdapter.notNull { adapter ->
            adapter.addEventHook(TodoItem.TodoUpdateItemClick { _, item ->
                viewModel.goToDetailsFragment(item)
            })
        }
        fastAdapter.notNull { adapter ->
            adapter.addEventHook(TodoItem.TodoItemFavClick { _, item ->
                viewModel.addTodoToFavourites(item.copy(favourite = !item.favourite))
                //fastAdapter.notifyItemChanged(position)
            })
        }
    }
}