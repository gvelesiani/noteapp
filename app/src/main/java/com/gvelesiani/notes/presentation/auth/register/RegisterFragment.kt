package com.gvelesiani.notes.presentation.auth.register

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.app3null.common_code.extensions.showThrowableMessage
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.fondesa.kpermissions.rx3.observe
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.gvelesiani.notes.R
import com.gvelesiani.notes.presentation.MainActivity
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.common.extensions.notNull
import com.zuluft.mvvm.dialogs.Loader
import com.zuluft.mvvm.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_register.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*


@LayoutResId(R.layout.fragment_register)
class RegisterFragment : BaseFragment<RegisterViewState, RegisterViewModel>() {

    private var selectedUri: Uri? = null
    private lateinit var auth: FirebaseAuth
    private val viewModel: RegisterViewModel by viewModel()

    override fun provideViewModel(): RegisterViewModel {
        return viewModel
    }

    override fun reflectState(viewState: RegisterViewState) {
        viewState.showLoader?.getValue().notNull {
            registerDialog(Loader.show(requireContext()))
        }

        viewState.showError?.getValue().notNull {
            dismissAndClearDialogs()
            requireContext().showThrowableMessage(it)
        }
    }

    override fun renderView(savedInstanceState: Bundle?) {
        auth = Firebase.auth
        btnNotNew.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }

        btnUploadPhoto.setOnClickListener {
            val request = permissionsBuilder(READ_EXTERNAL_STORAGE).build()
            request.observe().subscribe {
                // Handle the result.
                getImage()
            }
            request.send()

        }


        btnRegister.setOnClickListener {
            registerUser()
        }

    }


    private fun getImage() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, 0)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            selectedUri = data.data
        }
    }

    private fun registerUser() {
        auth.createUserWithEmailAndPassword(
            etRegisterEmailField.text.toString(),
            etRegisterPasswordField.text.toString()
        )
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = Firebase.auth.currentUser
                    val profileUpdates = userProfileChangeRequest {
                        this.displayName = etRegisterUserNameField.text.toString()
                    }

                    user!!.updateProfile(profileUpdates)
                        .addOnCompleteListener { t ->
                            if (t.isSuccessful) {
                                d("update user", "User profile updated.")
                            }
                        }
                    uploadImageToFirebaseStorage(user)
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    d("register tag", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(
                        requireContext(), "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                    updateUI(null)
                }
            }
    }

    private fun uploadImageToFirebaseStorage(user: FirebaseUser?) {
        if(selectedUri == null) return
        val fileName = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$fileName")
        ref.putFile(selectedUri!!).addOnSuccessListener {
            ref.downloadUrl.addOnSuccessListener {
                val profileUpdates = userProfileChangeRequest {
                    this.photoUri = it
                }
                user!!.updateProfile(profileUpdates)
                    .addOnCompleteListener { t ->
                        if (t.isSuccessful) {
                            d("update user", "Successfully updated Image for user")
                        }
                    }
            }
        }
    }

    private fun updateUI(account: FirebaseUser?) {
        if (account != null) {
            Toast.makeText(requireContext(), "Registered successfully", Toast.LENGTH_LONG).show()
            startActivity(Intent(requireContext(), MainActivity::class.java))
            requireActivity().finish()
        } else {
            Toast.makeText(requireContext(), "Failure", Toast.LENGTH_LONG).show()
        }
    }
}