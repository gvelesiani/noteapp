package com.gvelesiani.notes.presentation.biometric_authentication

import android.app.KeyguardManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import com.google.firebase.auth.FirebaseAuth
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.activities.BaseActivity
import com.gvelesiani.notes.R
import com.gvelesiani.notes.presentation.MainActivity
import com.gvelesiani.notes.presentation.auth.AuthActivity
import kotlinx.android.synthetic.main.activity_biometric_prompt.*
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.activity_biometric_prompt)
class BiometricPromptActivity : BaseActivity<BiometricPromptViewState, BiometricPromptViewModel>() {
    private var cancellationSignal: CancellationSignal? = null
    private val authenticationCallback: BiometricPrompt.AuthenticationCallback
        get() =
            @RequiresApi(Build.VERSION_CODES.P)
            object : BiometricPrompt.AuthenticationCallback(){
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
                    super.onAuthenticationError(errorCode, errString)
                    notifyUser("Authentication error: $errString")
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?) {
                    super.onAuthenticationSucceeded(result)
                    notifyUser("Authentication success!")
                    startActivity(Intent(this@BiometricPromptActivity, MainActivity::class.java ))
                }
            }

    private val viewModel: BiometricPromptViewModel by viewModel()

    override fun provideViewModel(): BiometricPromptViewModel {
        return viewModel
    }

    override fun reflectState(viewState: BiometricPromptViewState) {

    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun renderView(savedInstanceState: Bundle?) {
        val text = etPinCode.text
        btnOne.setOnClickListener {
            text?.append(btnOne.text)
        }

        btnZero.setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "application/pdf"
            }
            startActivityForResult(intent, 10)
        }

        checkBiometricSupport()

        btnBiometric.setOnClickListener {
            val biometricPrompt = BiometricPrompt.Builder(this)
                .setTitle("2DO Biometric Authentication")
                .setDescription("App uses fingerprint protection, to protect your data")
                .setNegativeButton("Cancel", this.mainExecutor, DialogInterface.OnClickListener { _, _ ->
                    notifyUser("Authentication canceled")
                    FirebaseAuth.getInstance().signOut()
                    startActivity(Intent(this@BiometricPromptActivity, AuthActivity::class.java))
                    finish()

                }).build()
            biometricPrompt.authenticate(getCancellationSignal(), mainExecutor, authenticationCallback)
        }

    }

    private fun getCancellationSignal(): CancellationSignal {
        cancellationSignal = CancellationSignal()
        cancellationSignal?.setOnCancelListener {
            notifyUser("Auth has canceled")
        }

        return cancellationSignal as CancellationSignal
    }

    private fun checkBiometricSupport() : Boolean {
        val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager

        if(!keyguardManager.isKeyguardSecure){
            notifyUser("Fingerprint auth has not been enabled in settings")
            return false
        }

        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.USE_BIOMETRIC) != PackageManager.PERMISSION_GRANTED) {
            notifyUser("Fingerprint auth permission isn't enabled")
            return false
        }

        return if(packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
            true
        } else true
    }

    private fun notifyUser(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}