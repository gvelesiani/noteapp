package com.gvelesiani.notes.other.diffCallback

import com.gvelesiani.notes.other.items.TodoItem
import com.mikepenz.fastadapter.diff.DiffCallback

class NoteListDiffCallback : DiffCallback<TodoItem> {
    override fun areContentsTheSame(oldItem: TodoItem, newItem: TodoItem): Boolean {
        return oldItem.model == newItem.model
    }

    override fun areItemsTheSame(oldItem: TodoItem, newItem: TodoItem): Boolean {
        return oldItem.model.id == newItem.model.id
    }

    override fun getChangePayload(
        oldItem: TodoItem,
        oldItemPosition: Int,
        newItem: TodoItem,
        newItemPosition: Int
    ): Boolean {
        return oldItem == newItem
    }
}