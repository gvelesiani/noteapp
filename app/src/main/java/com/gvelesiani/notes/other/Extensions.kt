package com.gvelesiani.notes.other

import android.os.Bundle
import androidx.navigation.NavController
import com.zuluft.mvvm.common.extensions.notNull
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T : Any> Single<T>.async(): Single<T> {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun Completable.async(): Completable {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun NavController.navigate(resId: Int, bundle: Bundle = Bundle(), clearStack: Boolean = false, disableReopening: Boolean = false) {
    if (disableReopening) {
        if (currentDestination == graph.findNode(resId)) {
            return
        }
    }
    if (clearStack) {
        currentDestination.notNull { popBackStack(it.id, true) }
    }
    navigate(resId, bundle)
}

