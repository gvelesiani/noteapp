package com.gvelesiani.notes.other.items

import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.gvelesiani.notes.R
import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.android.synthetic.main.todo_custom_layout.view.*

// IDraggable
class TodoItem(var model: TodoModel) : AbstractItem<RecyclerView.ViewHolder>() {
    override val layoutRes: Int = R.layout.todo_custom_layout
    override val type: Int = 1

//    override var isDraggable = true
//
//    fun withIsDraggable(draggable: Boolean): TodoItem {
//        this.isDraggable = draggable
//        return this
//    }

    override fun getViewHolder(v: View) =
        ViewHolder(v)

    class ViewHolder(private val view: View) :
        FastAdapter.ViewHolder<TodoItem>(view) {

        override fun bindView(item: TodoItem, payloads: List<Any>) {
            view.tvTodoName.text = item.model.todoName
            view.btnMakeNoteFavourite.isSelected = item.model.favourite
            view.btnTodoDone.isSelected = item.model.done
        }

        override fun unbindView(item: TodoItem) {
        }
    }

    class TodoUpdateItemClick(
        private val onClickListener: (Int, TodoModel) -> Unit
    ) : ClickEventHook<TodoItem>() {

        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return if (viewHolder is ViewHolder) {
                return viewHolder.itemView.btnUpdate
            } else {
                null
            }
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<TodoItem>,
            item: TodoItem
        ) {
            onClickListener.invoke(position, item.model)
        }
    }

    class TodoDeleteClick(
        private val onClickListener: (Int, TodoModel) -> Unit
    ) : ClickEventHook<TodoItem>() {

        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return if (viewHolder is ViewHolder) {
                return viewHolder.itemView.btnDelete
            } else {
                null
            }
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<TodoItem>,
            item: TodoItem
        ) {
            onClickListener.invoke(position, item.model)
        }
    }

    class TodoItemFavClick(
        private val onClickListener: (Int, TodoModel) -> Unit
    ) : ClickEventHook<TodoItem>() {

        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return if (viewHolder is ViewHolder) {
                return viewHolder.itemView.btnMakeNoteFavourite
            } else {
                null
            }
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<TodoItem>,
            item: TodoItem
        ) {
            onClickListener.invoke(position, item.model)
        }
    }


    class TodoItemDoneClick(
        private val onClickListener: (Int, TodoModel) -> Unit
    ) : ClickEventHook<TodoItem>() {

        override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
            return if (viewHolder is ViewHolder) {
                return viewHolder.itemView.btnTodoDone
            } else {
                null
            }
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<TodoItem>,
            item: TodoItem
        ) {
            onClickListener.invoke(position, item.model)
        }
    }

}