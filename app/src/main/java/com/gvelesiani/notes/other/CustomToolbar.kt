package com.gvelesiani.notes.other

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.app3null.common_code.extensions.notNull
import com.gvelesiani.notes.R
import com.makeramen.roundedimageview.RoundedImageView

class CustomToolbar : ConstraintLayout {

    constructor(context: Context) : super(context) {
        initLayout(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initLayout(context, attrs)
    }

    private lateinit var tvMonth: AppCompatTextView
    private lateinit var tvDayOfWeek: AppCompatTextView
    private lateinit var tvDayOfMonth: AppCompatTextView
    private lateinit var ivAvatar: RoundedImageView
    private lateinit var tvTaskSize: AppCompatTextView

    private var textDate: String? = null

    private fun initLayout(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.custom_toolbar_layout, this)
        tvMonth = findViewById(R.id.tvCurrentMonth)
        ivAvatar = findViewById(R.id.ivAvatar)
        tvDayOfWeek = findViewById(R.id.tvCurrentDayOfWeek)
        tvDayOfMonth = findViewById(R.id.tvCurrentDayDate)
        tvTaskSize = findViewById(R.id.tvTasksNumber)

        attrs.notNull {
            val array = context.obtainStyledAttributes(attrs, R.styleable.CustomToolbar)

            if (array.hasValue(R.styleable.CustomToolbar_ct_date)) {
                textDate = array.getString(R.styleable.CustomToolbar_ct_date)
            }

//            if (hasHomeButton) {
//                ivButton.setImageResource(R.drawable.ic_home)
//            } else {
//                ivButton.setImageResource(R.drawable.ic_settings)
//            }

            tvMonth.text = textDate
            array.recycle()
        }
    }
}