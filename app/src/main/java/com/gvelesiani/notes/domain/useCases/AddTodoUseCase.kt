package com.gvelesiani.notes.domain.useCases

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.repository.TodoRepository
import io.reactivex.Completable

class AddTodoUseCase(repository: TodoRepository, private val getTodoListSizeUseCase: GetTodoListSizeUseCase): BaseUseCase<TodoModel, Completable>(repository) {
    override fun start(arg: TodoModel?): Completable {
        return getTodoListSizeUseCase.start()
            .flatMapCompletable {
                arg?.position = it
                repository.addTodo(arg!!)
            }
    }
}