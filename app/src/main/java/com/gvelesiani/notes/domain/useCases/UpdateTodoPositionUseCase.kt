package com.gvelesiani.notes.domain.useCases

import android.util.Log.d
import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.repository.TodoRepository
import io.reactivex.Completable

class UpdateTodoPositionUseCase(repository: TodoRepository): BaseUseCase<Pair<TodoModel, Int>, Completable>(repository) {
    override fun start(arg: Pair<TodoModel, Int>?): Completable {
        d("dbg", "positionUseCase start")
        val diapason = getDiapason(arg?.first!!.position, arg.second)
        return repository.getInDiapason(diapason.first, diapason.second).flatMapCompletable {
            val newList = it.map{it.copy(position = if(arg.first.position < arg.second) it.position - 1 else it.position + 1)}.toMutableList()
            newList.add(arg.first.copy(position = arg.second))
            d("dbg", "in useCase")
            repository.updateTodosPosition(newList)
        }
    }

    private fun getDiapason(firstPosition: Int, secondPosition: Int) : Pair<Int, Int>{
        if(firstPosition < secondPosition){
            return Pair(firstPosition + 1, secondPosition)
        } else {
            return Pair(secondPosition, firstPosition - 1)
        }
    }
}