package com.gvelesiani.notes.domain.dataProviders.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "todo_table")
data class TodoModel(
    @PrimaryKey
    var id: String = "",
    var position: Int = 0,
    //@SerializedName("noteName")
    var todoName: String ="",
    //@SerializedName("isFavourite")
    var favourite: Boolean = false,
    var userUid: String ="",
    val done: Boolean = false

) : Parcelable