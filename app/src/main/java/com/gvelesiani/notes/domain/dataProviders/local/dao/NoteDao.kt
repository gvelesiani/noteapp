package com.gvelesiani.notes.domain.dataProviders.local.dao

import androidx.room.*
import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import io.reactivex.Observable
import io.reactivex.Single


// contains methods used for accessing the database
@Dao
interface NoteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTodo(todo: TodoModel)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAllPosts(todo: List<TodoModel>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(todo: List<TodoModel>): List<Long>

    @Update
    abstract fun update(todo: List<TodoModel>)

    @Transaction
    open fun insertOrUpdate(todoList: List<TodoModel>) {
        val insertResult = insert(todoList)
        val updateList = mutableListOf<TodoModel>()
        for (i in insertResult.indices) {
            if (insertResult[i] == -1L) updateList.add(todoList[i])
        }
        if (updateList.isNotEmpty()) update(updateList)
    }

    @Query("SELECT * FROM todo_table WHERE position between :startPosition and :endPosition ORDER BY position")
    fun getInDiapason(startPosition: Int, endPosition: Int): Single<List<TodoModel>>


    @Transaction
    open fun updateAll(todos: List<TodoModel>) {
        deleteAllTodos()
        return insertAllPosts(todos)
    }

    @Query("UPDATE todo_table SET favourite=:isFavourite where id=:id")
    fun addNoteToFavourites(isFavourite: Boolean, id: String)

    @Query("UPDATE todo_table SET done=:isDone where id=:id")
    fun todoDone(isDone: Boolean, id: String)

    @Query("SELECT * FROM todo_table where favourite=:isFavourite")
    fun getAllFavouriteNotes(isFavourite: Boolean = true): Observable<List<TodoModel>>

    @Query("UPDATE todo_table SET todoName=:todoName WHERE position=:position")
    fun updateTodo(position:Int, todoName: String)

    @Delete
    fun deleteTodo(todo: TodoModel)

    @Query("DELETE FROM todo_table")
    fun deleteAllTodos()

    @Query("SELECT * FROM todo_table ORDER BY position")
    fun getAllData(): Observable<List<TodoModel>>

    @Query("SELECT COUNT(id) FROM todo_table")
    fun getListSize(): Single<Int>

}