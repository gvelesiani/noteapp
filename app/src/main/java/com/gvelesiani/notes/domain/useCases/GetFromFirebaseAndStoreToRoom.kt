package com.gvelesiani.notes.domain.useCases

import com.gvelesiani.notes.domain.repository.TodoRepository
import io.reactivex.Observable

class GetFromFirebaseAndStoreToRoom(repository: TodoRepository, private val saveTodosToRoom: SaveTodosToRoom): BaseRxUseCase<Unit, Boolean>(repository){
    override fun start(arg: Unit?): Observable<Boolean> {
        return repository.getTodosFromFirebase()
            .flatMap {
                saveTodosToRoom.start(it)
            }
    }
}