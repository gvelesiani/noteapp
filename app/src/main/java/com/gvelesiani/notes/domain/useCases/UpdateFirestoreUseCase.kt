package com.gvelesiani.notes.domain.useCases

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.repository.TodoRepository
import io.reactivex.Completable

class UpdateFirestoreUseCase(repository: TodoRepository): BaseUseCase<TodoModel, Completable>(repository) {
    override fun start(arg: TodoModel?): Completable {
        return repository.updateOnFirestore(arg!!)
    }
}