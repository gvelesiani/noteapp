package com.gvelesiani.notes.domain.useCases

import com.gvelesiani.notes.domain.repository.TodoRepository
import io.reactivex.Single

class GetTodoListSizeUseCase(repository: TodoRepository): BaseUseCase<Unit, Single<Int>>(repository) {
    override fun start(arg: Unit?): Single<Int> {
        return repository.getListSize()
    }
}