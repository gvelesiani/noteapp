package com.gvelesiani.notes.domain.useCases

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.repository.TodoRepository
import io.reactivex.Observable

class TodoDoneUseCase(repository: TodoRepository): BaseRxUseCase<TodoModel, Boolean>(repository) {
    override fun start(arg: TodoModel?): Observable<Boolean> {
        return repository.todoDone(arg!!)
    }
}