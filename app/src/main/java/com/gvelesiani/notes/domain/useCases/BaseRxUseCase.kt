package com.gvelesiani.notes.domain.useCases

import com.gvelesiani.notes.domain.repository.TodoRepository
import io.reactivex.Observable
import com.zuluft.mvvm.usecases.BaseUseCase

abstract class BaseRxUseCase<ARG_TYPE, RETURN_TYPE>(repository: TodoRepository) :
    BaseUseCase<TodoRepository, ARG_TYPE, Observable<RETURN_TYPE>>(repository)
