package com.gvelesiani.notes.domain.dataProviders.global

import android.util.Log
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.firestore.WriteBatch
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.dataProviders.models.UserModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe

class GlobalDataProviderImpl : GlobalDataProvider {
    override fun addTodoToFirestore(todo: TodoModel): Completable {
        return Completable.fromCallable {
//            Firebase.firestore.collection("notes").add(todo).addOnCompleteListener {
//                //Firebase.firestore.collection("notes").document(it.result!!.id)
////                    .update("id", it.result!!.id)
//            }
            Firebase.firestore.collection("users").document(Firebase.auth.currentUser!!.uid)
                .collection("notes").add(todo).addOnCompleteListener {
                Firebase.firestore.collection("notes").document(it.result!!.id).update("id", it.result!!.id)
            }
        }
    }

    override fun updateTodoInFirestore(todo: TodoModel): Completable {
        return Completable.fromCallable {
            Firebase.firestore.collection("users").document(Firebase.auth.currentUser!!.uid).collection("notes").document(todo.id).update(
                mapOf(
                    "todoName" to todo.todoName
                )
            )
        }
    }

    override fun getTodosFromFirebase(): Observable<List<TodoModel>> {
        return Observable.create { emitter ->
            Firebase.firestore.collection("users").document(Firebase.auth.currentUser!!.uid).collection("notes").addSnapshotListener { value, _ ->
                val list = mutableListOf<TodoModel>()
                for (d in value?.documents!!) {
                    list.add(
                        TodoModel(
                            d.id,
                            d.getLong("position")!!.toInt(),
                            d.getString("todoName")!!,
                            d.getBoolean("favourite")!!,
                            d.getString("userUid")!!,
                            d.getBoolean("done")!!
                        )
                    )
                }

                emitter.onNext(list)
            }
        }
    }

    override fun updateTodosPosition(todo: List<TodoModel>): Completable {
        return Completable.fromCallable {
            val db = Firebase.firestore
            val batch: WriteBatch = db.batch()
            for (td in todo) {
                batch.update(
                    Firebase.firestore.collection("users").document(Firebase.auth.currentUser!!.uid).collection("notes").document(td.id),
                    "position",
                    td.position
                )
            }
            batch.commit().addOnCompleteListener {
                if (it.isSuccessful) {
                }
            }
        }
    }

    override fun addTodoToFavourites(todo: TodoModel): Observable<Boolean> {
        return Observable.fromCallable {
            Firebase.firestore.collection("users").document(Firebase.auth.currentUser!!.uid).collection("notes").document(todo.id).update(
                mapOf(
                    "favourite" to !todo.favourite
                )
            )
            true
        }
    }

    override fun addTodoToDone(todo: TodoModel): Observable<Boolean> {
        return Observable.fromCallable {
            Firebase.firestore.collection("users").document(Firebase.auth.currentUser!!.uid).collection("notes").document(todo.id).update(
                mapOf(
                    "done" to !todo.done
                )
            )
            true
        }
    }

    override fun deleteTodoFromDb(todo: TodoModel): Completable {
        return Completable.fromCallable {
            Firebase.firestore.collection("users").document(Firebase.auth.currentUser!!.uid).collection("notes").document(todo.id).delete()
            true
        }
    }

    override fun createUser(user: UserModel): Completable {
        return Completable.fromCallable {
            val auth = Firebase.auth
            auth.createUserWithEmailAndPassword(
                user.email,
                user.password
            )
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("register tag", "createUserWithEmail:success")
                        val currentUser = Firebase.auth.currentUser
                        val profileUpdates = userProfileChangeRequest {
                            this.displayName = user.username
                        }
                        currentUser!!.updateProfile(profileUpdates)
                            .addOnCompleteListener {
                                if (it.isSuccessful) {
                                    Log.d("update user", "User profile updated.")
                                }
                            }
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.d("register tag", "createUserWithEmail:failure", task.exception)
                    }
                }
        }
    }

}