package com.gvelesiani.notes.domain.dataProviders.global

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.dataProviders.models.UserModel
import io.reactivex.Completable
import io.reactivex.Observable


interface GlobalDataProvider {
    fun addTodoToFirestore(todo: TodoModel): Completable
    fun updateTodoInFirestore(todo: TodoModel): Completable
    fun getTodosFromFirebase(): Observable<List<TodoModel>>
    fun updateTodosPosition(todo: List<TodoModel>): Completable
    fun addTodoToFavourites(todo: TodoModel): Observable<Boolean>
    fun addTodoToDone(todo: TodoModel): Observable<Boolean>
    fun deleteTodoFromDb(todo: TodoModel): Completable
    fun createUser(user: UserModel): Completable
}