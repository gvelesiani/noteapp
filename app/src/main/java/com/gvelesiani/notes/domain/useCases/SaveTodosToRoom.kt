package com.gvelesiani.notes.domain.useCases

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.repository.TodoRepository
import io.reactivex.Observable

class SaveTodosToRoom(repository: TodoRepository):
    BaseRxUseCase<List<TodoModel>, Boolean>(repository) {
    override fun start(arg: List<TodoModel>?): Observable<Boolean> {
        return repository.insertOrUpdate(arg!!)
    }
}
