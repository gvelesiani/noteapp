package com.gvelesiani.notes.domain.dataProviders.local

import com.gvelesiani.notes.domain.dataProviders.local.database.TodoDatabase
import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.zuluft.mvvm.preferences.Preferences
import io.reactivex.Observable
import io.reactivex.Single

class LocalDataProviderImpl constructor(
    private val preferences: Preferences,
    private val database: TodoDatabase
) : LocalDataProvider{
    override fun addNoteToFavourites(todo: TodoModel): Observable<Boolean> {
        return Observable.fromCallable {
            database.getTodoDao().addNoteToFavourites(todo.favourite, todo.id)
            true
        }
    }

    override fun todoDone(todo: TodoModel): Observable<Boolean> {
        return Observable.fromCallable {
            database.getTodoDao().todoDone(todo.done, todo.id)
            true
        }
    }

    override fun getAllTodos(): Observable<List<TodoModel>> {
        return database.getTodoDao().getAllData()
    }

    override fun addTodo(todo: TodoModel) : Observable<Boolean>{
        return Observable.fromCallable {
            database.getTodoDao().addTodo(todo)
            true
        }
    }

    override fun saveGlobalPostsToDatabase(todo: List<TodoModel>): Observable<Boolean> {
        return Observable.fromCallable {
            database.getTodoDao().insertAllPosts(todo)
            true
        }
    }

    override fun updateTodo(todo: TodoModel): Observable<Boolean> {
        return Observable.fromCallable {
            database.getTodoDao().updateTodo(todo.position, todo.todoName)
            true
        }
    }

    override fun deleteAllTodos() {
        return database.getTodoDao().deleteAllTodos()
    }

    override fun deleteTodo(todo: TodoModel): Observable<Boolean> {
        return Observable.fromCallable {
            database.getTodoDao().deleteTodo(todo)
            true
        }
    }

    override fun getFavourites(): Observable<List<TodoModel>> {
        return database.getTodoDao().getAllFavouriteNotes()
    }

    override fun updateAll(todos: List<TodoModel>): Observable<Boolean> {
        return Observable.fromCallable {
            database.getTodoDao().updateAll(todos)
            true
        }
    }

    override fun getListSize(): Single<Int> {
        return database.getTodoDao().getListSize()
    }

    override fun insertOrUpdate(list: List<TodoModel>) {
        return database.getTodoDao().insertOrUpdate(list)
    }

    override fun getInDiapason(startPosition: Int, endPosition: Int): Single<List<TodoModel>> {
        return database.getTodoDao().getInDiapason(startPosition, endPosition)
    }


}