package com.gvelesiani.notes.domain.useCases

import com.gvelesiani.notes.domain.repository.TodoRepository
import com.zuluft.mvvm.usecases.BaseUseCase

abstract class BaseUseCase<ARG_TYPE, RETURN_TYPE>(repository: TodoRepository) :
    BaseUseCase<TodoRepository, ARG_TYPE, RETURN_TYPE>(repository)