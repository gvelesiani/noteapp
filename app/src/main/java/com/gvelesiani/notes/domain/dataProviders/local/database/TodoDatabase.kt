package com.gvelesiani.notes.domain.dataProviders.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gvelesiani.notes.domain.dataProviders.local.dao.NoteDao
import com.gvelesiani.notes.domain.dataProviders.models.TodoModel


@Database(
    entities = [TodoModel::class],
    version = 63,
    exportSchema = false
)

abstract class TodoDatabase : RoomDatabase(){
    abstract fun getTodoDao(): NoteDao
}