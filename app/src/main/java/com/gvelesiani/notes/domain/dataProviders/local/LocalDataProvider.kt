package com.gvelesiani.notes.domain.dataProviders.local

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import io.reactivex.Observable
import io.reactivex.Single

interface LocalDataProvider {
    fun addNoteToFavourites(todo: TodoModel): Observable<Boolean>
    fun todoDone(todo: TodoModel): Observable<Boolean>
    fun getAllTodos(): Observable<List<TodoModel>>
    fun addTodo(todo: TodoModel): Observable<Boolean>
    fun saveGlobalPostsToDatabase(todo: List<TodoModel>): Observable<Boolean>
    fun updateTodo(todo: TodoModel): Observable<Boolean>
    fun deleteAllTodos()
    fun deleteTodo(todo:TodoModel) : Observable<Boolean>
    fun getFavourites(): Observable<List<TodoModel>>
    fun updateAll(todos: List<TodoModel>): Observable<Boolean>
    fun getListSize(): Single<Int>
    fun insertOrUpdate(list: List<TodoModel>)
    fun getInDiapason(startPosition: Int, endPosition: Int): Single<List<TodoModel>>
}