package com.gvelesiani.notes.domain.repository

import android.util.Log.d
import com.gvelesiani.notes.domain.dataProviders.global.GlobalDataProvider
import com.gvelesiani.notes.domain.dataProviders.local.LocalDataProvider
import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single


class TodoRepositoryImpl(
    private val localDataProvider: LocalDataProvider,
    private val globalDataProvider: GlobalDataProvider
) : TodoRepository {

    override fun getTodoList(): Observable<List<TodoModel>> {
        return localDataProvider.getAllTodos().map {
            it
        }
    }


    override fun addTodo(todo: TodoModel): Completable {
        return globalDataProvider.addTodoToFirestore(todo)
    }

    override fun deleteTodo(todo: TodoModel): Observable<Boolean> {
        return localDataProvider.deleteTodo(todo)
    }

    override fun updateTodo(todo: TodoModel): Observable<Boolean> {
        return localDataProvider.updateTodo(
            TodoModel(
                position = todo.position,
                todoName = todo.todoName
            )
        )
    }

    override fun deleteAllTodos() {
        return localDataProvider.deleteAllTodos()
    }

    override fun addTodoToFavourites(todo: TodoModel): Observable<Boolean> {
        return globalDataProvider.addTodoToFavourites(todo)
    }

    override fun getAllFavourites(): Observable<List<TodoModel>> {
        return localDataProvider.getFavourites()
    }

    override fun todoDone(todo: TodoModel): Observable<Boolean> {
        return globalDataProvider.addTodoToDone(todo)
    }

    override fun getTodosFromFirebase(): Observable<List<TodoModel>> {
        return globalDataProvider.getTodosFromFirebase()
    }

    override fun saveGlobalTodosToDatabase(todos: List<TodoModel>): Observable<Boolean> {
        return localDataProvider.saveGlobalPostsToDatabase(todos)
    }

    override fun updateAll(todos: List<TodoModel>): Observable<Boolean> {
        return localDataProvider.updateAll(todos).map {
            it
        }
    }

    override fun getListSize(): Single<Int> {
        return localDataProvider.getListSize().map {
            it
        }
    }

    override fun updateOnFirestore(todo: TodoModel): Completable {
        return globalDataProvider.updateTodoInFirestore(todo)
    }

    override fun insertOrUpdate(list: List<TodoModel>): Observable<Boolean> {
        return Observable.fromCallable {
            localDataProvider.insertOrUpdate(list)
            true
        }
    }

    override fun updateTodosPosition(todo: List<TodoModel>): Completable {
        return globalDataProvider.updateTodosPosition(todo)
    }

    override fun getInDiapason(startPosition: Int, endPosition: Int): Single<List<TodoModel>> {
        return localDataProvider.getInDiapason(startPosition, endPosition)
    }

}