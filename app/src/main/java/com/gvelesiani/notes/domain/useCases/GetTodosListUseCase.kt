package com.gvelesiani.notes.domain.useCases

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import com.gvelesiani.notes.domain.repository.TodoRepository
import io.reactivex.Observable

class GetTodosListUseCase(repository: TodoRepository): BaseRxUseCase<TodoModel, List<TodoModel>>(repository){
    override fun start(arg: TodoModel?): Observable<List<TodoModel>> {
        return repository.getTodoList()
    }
}