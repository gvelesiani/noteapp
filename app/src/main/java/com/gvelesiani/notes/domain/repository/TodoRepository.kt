package com.gvelesiani.notes.domain.repository

import com.gvelesiani.notes.domain.dataProviders.models.TodoModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface TodoRepository {
    fun getTodoList(): Observable<List<TodoModel>>
    fun addTodo(todo: TodoModel): Completable
    fun deleteTodo(todo: TodoModel): Observable<Boolean>
    fun updateTodo(todo: TodoModel): Observable<Boolean>
    fun deleteAllTodos()
    fun addTodoToFavourites(todo: TodoModel): Observable<Boolean>
    fun getAllFavourites(): Observable<List<TodoModel>>
    fun todoDone(todo: TodoModel): Observable<Boolean>
    fun getTodosFromFirebase(): Observable<List<TodoModel>>
    fun saveGlobalTodosToDatabase(todos: List<TodoModel>): Observable<Boolean>
    fun updateAll(todos: List<TodoModel>): Observable<Boolean>
    fun getListSize(): Single<Int>
    fun updateOnFirestore(todo: TodoModel): Completable
    fun insertOrUpdate(list: List<TodoModel>): Observable<Boolean>
    fun updateTodosPosition(todo: List<TodoModel>): Completable
    fun getInDiapason(startPosition: Int, endPosition: Int): Single<List<TodoModel>>
}
