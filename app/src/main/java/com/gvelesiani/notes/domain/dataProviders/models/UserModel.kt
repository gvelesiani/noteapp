package com.gvelesiani.notes.domain.dataProviders.models

data class UserModel(
    var email: String,
    var username: String,
    var password: String = "",
    var uid: String
)